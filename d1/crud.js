const http = require("http");
const port = 6000;

//mock database
let directory = [
    {
        name: "Brandon",
        email: "brandon@mail.com",
    },
    {
        name: "Jobert",
        email: "jobert@mail.com",
    }
]

console.log(typeof directory);

const server = http.createServer((req, res) => {

    // route for returning all itms upon recieving a GET request
    if(req.url == '/users' && req.method == "GET"){
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.write(JSON.stringify(directory));
        res.end()
    }

    // Create users
    if(req.url == '/users' && req.method == "POST"){

        //initialize variable for placeholder create empty string.
        let requestBody = ''; 

        //A "stream" is a sequence of data

        //data step - this is the first sequence of stream. this read the actual data stream and process it as the request body. 
        //The information provided from the request object(client) enters a sequence of "data".
        req.on('data', function(data){
            //Assigns the data retrieved from the data stream to requestBody
            requestBody += data;

        });

        //response end step - only runs after the request has completely been sent
        req.on('end', function(){
            console.log(typeof requestBody);
            //convert the string requestBody to JSON
            requestBody = JSON.parse(requestBody)

            //Create new object representing the new mock database record
            let newUser = {
                "name": requestBody.name,
                "email": requestBody.email
            }
            //Add the new user into the mock database
            directory.push(newUser);
            console.log(directory);

            res.writeHead(200, {'Content-Type': 'application/json'});
            res.write(JSON.stringify(newUser));
            res.end()
        })

    }
    






});

server.listen(port);

console.log(`Server running at localhost: ${port}`);